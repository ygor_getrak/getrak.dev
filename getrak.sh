#!/bin/bash

function clone() {
	git clone git@bitbucket.org:getrak/estatico.git
	git clone git@bitbucket.org:getrak/getrak.git
	git clone git@bitbucket.org:getrak/getrak-api.git
	git clone git@bitbucket.org:getrak/laradock-fork.git
	git clone git@bitbucket.org:getrak/core.git
	git clone git@bitbucket.org:getrak/crudmicro.git
	git clone git@bitbucket.org:getrak/infraservice.git
}

function config() {
	cp laradock-fork.env laradock-fork/.env
	cp getrak.env getrak/.env
	cp getrak-api.env getrak-api/.env
}

function up() {
	pushd laradock-fork
	docker-compose up --detach nginx mysql redis phpmyadmin
	popd
}

function database() {
	pushd laradock-fork
	docker-compose exec -T mysql mysql -proot <<-SQL
		CREATE DATABASE gdesk;
		CREATE DATABASE satcompany;
	SQL
	popd
}

function dependencies() {
	pushd laradock-fork
	PHP_PROJECTS=(getrak getrak-api core)
	for PROJECT in ${PHP_PROJECTS[*]}; do
		docker-compose exec -T \
			--user=laradock \
			workspace bash -x <<-EOF
				cd $PROJECT
				composer install
				composer dumpautoload
			EOF
	done
	popd
}

function migrate() {
	pushd laradock-fork
	docker-compose exec -T \
		--user=laradock \
		workspace bash -x <<-EOF
			cd /var/www/getrak-api
			php artisan  migrate --path=database/migrations/gdesk
			php artisan  migrate --path=database/migrations/satcompany
			php artisan  migrate
		EOF
	popd
}

# function check() {}

function infraservice() {
	docker run --name=infraservice -p 9000:8080 -v /$PWD/infraservice:/app -w //app openjdk:8 ./gradlew bootRun --args='--spring.profiles.active=dev'
}

$1